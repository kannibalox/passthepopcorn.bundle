import re
import os
from lxml.etree import tostring
from lxml import html
from lxml.html import fromstring

PREFIX = '/video/passthepopcorn'

ART = 'art-default.jpg'
ICON = 'icon-default.jpg'

"""
view_modes = {
  "List": 65586, "InfoList": 65592, "MediaPreview": 458803, "Showcase": 458810, "Coverflow": 65591,
  "PanelStream": 131124, "WallStream": 131125, "Songs": 65593, "Seasons": 65593, "Albums": 131123,
  "Episodes": 65590,"ImageStream":458809,"Pictures":131123
}
"""

ORDER_DICT = {
    "Time Added": '', 
    "Year": '&order_by=year',
    "PTP Rating": '&order_by=ptprating',
    "IMDB Rating": '&order_by=imdb',
    "Title": '&order_by=title', 
    "Size": '&order_by=size'
}

@handler(PREFIX, 'PassThePopcorn', thumb=ICON)
def Main():
    HTTP.Headers['User-Agent'] = "Wget/1.13.4"
    oc = ObjectContainer(
        objects = [
            DirectoryObject(
                key = Callback(Latest),
                title = "Latest Movies"
                ),
            DirectoryObject(
                key = Callback(Bookmarks),
                title = "Bookmarks"
                ),
            DirectoryObject(
                key = Callback(GenreList),
                title = "Genres"
                ),
            DirectoryObject(
                key = Callback(Freeleech),
                title = "Freeleech"
                ),
            DirectoryObject(
                key = Callback(GoldenPopcorn),
                title = "Golden Popcorn"
                ),
            DirectoryObject(
                key = Callback(Top50),
                title = "Top 10"
                ),
            InputDirectoryObject(
                key = Callback(Search),
                title = "Search by Title",
                prompt = "Search by Title"
                ),
            PrefsObject(
                title = "Plug-in Preferences"
                )
            ]
        )
    if Dict['loggedIn']:
        return oc
    else:
        return ObjectContainer(objects = [
                DirectoryObject(
                    key = Callback(NullCallback),
                    title = "Please change preferences to log in"
                    ),
                PrefsObject(
                    title = "Plug-in Preferences"
                    )
                ])



def checkLogin():
    if not Prefs["username"] or not Prefs["password"] or not Prefs["passkey"]:
        Log.Debug("Not enough information included to log in")
        return False
    Dict['loggedIn'] = False
    Log.Debug("Username %s attempting to log in" % Prefs["username"])
    postData = {"username": Prefs["username"], "password": Prefs["password"], "passkey": Prefs["passkey"] }
    json = JSON.ObjectFromURL( "https://tls.passthepopcorn.me/ajax.php?action=login", postData )
    Log.Debug("Login response: %s" % json)
    if json['Result'] == 'Ok':
        Dict['loggedIn'] = True
    return Dict['loggedIn']

def NullCallback():
    pass

def ValidatePrefs():
    checkLogin()
    if not Dict['loggedIn']:
        return MessageContainer(title1="URL Error", title2="URL Error", message="Username %s did not login in successfully" % Prefs["username"], header="Login Error" % tID)

def Start():
    Dict['loggedIn'] = False
    checkLogin()
    Plugin.AddViewGroup("List", viewMode="List", mediaType="items", cols=50)
    Plugin.AddViewGroup("InfoList", viewMode="InfoList", mediaType="movies", cols=5, rows=5)

@route(PREFIX + '/latest') 
def Latest(page=1):
    oc = ObjectContainer(title1="Most Recent", title2="Recent Movies")
    movies = Torrents(page=int(page))
    for mov in movies:
        oc.add(mov)
    oc.add(NextPageObject(key=Callback(Latest, page=int(page)+1)))
    return oc

@route(PREFIX + '/freeleech')
def Freeleech(page=1):
    oc = ObjectContainer(title1="Freeleech", title2="Freeleech", view_group="InfoList", content="movies")
    movies = Torrents(query="freetorrent=1" + ORDER_DICT[Prefs['order_by']], page=int(page))
    for mov in movies:
        oc.add(mov)
    oc.add(NextPageObject(key=Callback(Freeleech, page=int(page)+1)))
    return oc

@route(PREFIX + '/goldenpopcorn')
def GoldenPopcorn(page=1):
    oc = ObjectContainer(title1="GoldenPopcorn", title2="GoldenPopcorn", view_group="InfoList", content="movies")
    movies = Torrents(query="scene=2" + ORDER_DICT[Prefs['order_by']], page=int(page))
    for mov in movies:
        oc.add(mov)
    oc.add(NextPageObject(key=Callback(GoldenPopcorn, page=int(page)+1)))
    return oc

@route(PREFIX + '/genrelist')
def GenreList():
    oc = ObjectContainer(title1="Genres", title2="Genres", view_group="List")
    for g in ['horror', 'action', 'drama', 'comedy', 'crime', 'western', 'martial.arts', 'film.noir', 'fantasy', 'animation', 'musical', 'western']:
        t = g.replace('.', ' ').title()
        oc.add(DirectoryObject(key=Callback(Genre, g=g), title=t))
    return oc
        
@route(PREFIX + '/genre/{g}')
def Genre(g, page=1):
    t = g.replace('.', ' ').title()
    oc = ObjectContainer(title1=t, title2=t, view_group="InfoList", content="movies")
    movies = Torrents(query="taglist=%s%s" % (g, ORDER_DICT[Prefs['order_by']]), page=int(page))
    for mov in movies:
        oc.add(mov)
    oc.add(NextPageObject(key=Callback(Genre, g=g, page=int(page)+1)))
    return oc

@route(PREFIX + '/search/{query}')
def Search(query, page=1):
    oc = ObjectContainer(title1="Search Results", title2=query, view_group="InfoList", content="movies")
    movies = Torrents(query="searchstr=%s%s" % (query, ORDER_DICT[Prefs['order_by']]), page=int(page))
    mov_cnt = 0
    for mov in movies:
        oc.add(mov)
        mov_cnt += 1
    if mov_cnt == 50:
        oc.add(NextPageObject(key=Callback(Search, query=query, page=int(page)+1)))
    return oc

@route(PREFIX + '/top50')
def Top50():
    oc = ObjectContainer(
        objects = [
            DirectoryObject(
                key = Callback(Top50List, span="day"),
                title = "Top 50 Movies - Day"
                ),
            DirectoryObject(
                key = Callback(Top50List, span="week"),
                title = "Top 50 Movies - Week"
                ),
            DirectoryObject(
                key = Callback(Top50List, span="month"),
                title = "Top 50 Movies - Month"
                ),
            DirectoryObject(
                key = Callback(Top50List, span="overall"),
                title = "Top 50 Movies - All Time"
                ),
            DirectoryObject(
                key = Callback(Top50List, span="snatched"),
                title = "Top 50 Movies - Snatched"
                ),
            DirectoryObject(
                key = Callback(Top50List, span="transferred"),
                title = "Top 50 Movies - Transferred"
                ),
            DirectoryObject(
                key = Callback(Top50List, span="seeded"),
                title = "Top 50 Movies - Seeded"
                ),
            ]
        )
    return oc

@route(PREFIX + '/top50/{span}')
def Top50List(span):
    oc = ObjectContainer(title1="Top 10 - %s" % span, title2="Top 10 Movies", view_group="InfoList", content="movies")
    movies = HTML.ElementFromURL("https://tls.passthepopcorn.me/top10.php?type=movies&limit=100&details=%s" % span)
    m = re.search(r"coverViewJsonData\[ . \] = (\{.*\});", tostring(movies))
    if m is None:
        oc.add(DirectoryObject(title="Could not find movie data", key=0))
        return oc
    j = JSON.ObjectFromString(m.group(1))
    if len(j['Movies']) == 0:
        oc.add(DirectoryObject(title="No movies found", key=0))
        return oc
    movs = []
    for mov in j['Movies'][0:50]:
        HD = 'HD: %s' % ', '.join(mov['TorrentSummary'].get('Hd', ['None']))
        SD = 'SD: %s' % ', '.join(mov['TorrentSummary'].get('Sd', ['None']))
        summary = "%s\r\n%s\r\n%s" % ( mov.get('Synopsis','No summary available'), HD, SD )
        cover_url = String.Encode(mov['Cover'])
        dirs = mov.get('Directors', [{'Name': 'Unknown'}])
        do = DirectoryObject(
            title = "%s [%s]" % (clean_html_ents(mov["Title"]), int(mov['Year'])),
            tagline = ', '.join( mov["Tags"]),
            summary = clean_html_ents(summary),
            #directors = [d['Name'] for d in dirs],
            thumb = Callback(Thumb, url=cover_url)
            )
        do.key = Callback(MovieListing, m=mov["GroupId"])
        oc.add(do)
    return oc
    

@route(PREFIX + '/bookmarks')
def Bookmarks(page=1):
    oc = ObjectContainer(title1="Bookmark", title2="Bookmarked Movies", view_group="InfoList", content="movies")
    movies = HTML.ElementFromURL("https://tls.passthepopcorn.me/bookmarks.php?page=%i" % int(page))
    m = re.search(r"coverViewJsonData\[ 0 \] = (\{.*\});", tostring(movies))
    if m is None:
        oc.add(DirectoryObject(title="Could not find movie data", key=0))
        Log.Debug(tostring(movies))
        return oc
    j = JSON.ObjectFromString(m.group(1))
    if len(j['Movies']) == 0:
        oc.add(DirectoryObject(title="No movies found", key=0))
        Log.Debug(j)
        return oc
    movs = []
    for mov in j['Movies']:
        HD = 'HD: %s' % ', '.join(mov['TorrentSummary'].get('Hd', ['None']))
        SD = 'SD: %s' % ', '.join(mov['TorrentSummary'].get('Sd', ['None']))
        summary = "%s\r\n%s\r\n%s" % ( mov.get('Synopsis','No summary available'), HD, SD )
        cover_url = String.Encode(mov['Cover'])
        dirs = mov.get('Directors', [{'Name': 'Unknown'}])
        do = DirectoryObject(
            title = "%s [%s]" % (clean_html_ents(mov["Title"]), int(mov['Year'])),
            tagline = ', '.join( mov["Tags"]),
            summary = clean_html_ents(summary),
            #directors = [d['Name'] for d in dirs],
            thumb = Callback(Thumb, url=cover_url)
            )
        do.key = Callback(MovieListing, m=mov["GroupId"])
        oc.add(do)
    oc.add(NextPageObject(key=Callback(Bookmarks, page=int(page)+1)))
    return oc

def Torrents(query="",page=1):
    HTTP.Headers['User-Agent'] = "Wget/1.13.4"
    movies = HTML.ElementFromURL("https://tls.passthepopcorn.me/torrents.php?page=%i&" % int(page) + query)
    m = re.search(r"coverViewJsonData\[ 0 \] = (\{.*\});", tostring(movies))
    if m is None:
        scripts = movies.xpath('.//script')
        if len(scripts) == 0:
            Log.Debug("No script sections found, dumping entire page")
            Log.Debug(tostring(movies))
        else:
            for s in scripts[5:]:
                Log.Debug(tostring(s))
        return [DirectoryObject(title="Could not find movie data", key=0)]
    j = JSON.ObjectFromString(m.group(1))
    if len(j['Movies']) == 0:
        Log.Debug(j)
        return [DirectoryObject(title="No movies found", key=0)]
    movs = []
    # Hack to provide more viewing options
    movs.append(MovieObject(
            key = Callback(MovieInfo, m=page),
            rating_key = page,
            summary = "",
            title = "Results"
            ))

    for mov in j['Movies']:
        try:
            HD = 'HD: %s' % ', '.join(mov['TorrentSummary'].get('Hd', ['None'])).replace('&amp;#10047;', 'GP')
            SD = 'SD: %s' % ', '.join(mov['TorrentSummary'].get('Sd', ['None'])).replace('&amp;#10047;', 'GP')
        except KeyError:
            HD = SD = ""
        summary = "%s\r\n%s\r\n%s" % (mov.get('Synopsis','No summary available'), HD, SD )
        cover_url = String.Encode(mov['Cover'])
        dirs = mov.get('Directors', [{'Name': 'Unknown'}])
        do = DirectoryObject(
            title = "%s [%s]" % (clean_html_ents(mov["Title"]), int(mov['Year'])),
            tagline = ', '.join(mov["Tags"]),
            summary = clean_html_ents(summary),
            thumb = Callback(Thumb, url=cover_url)
            )
        do.key = Callback(MovieListing, m=mov["GroupId"])
        movs.append(do)
    return movs

@route(PREFIX + '/movielisting/{m}')
def MovieListing(m):
    oc = ObjectContainer()
    try:
        mHTML = HTML.ElementFromURL('https://tls.passthepopcorn.me/torrents.php?id=%s' % m)
        mJSON = JSON.ObjectFromURL('https://tls.passthepopcorn.me/torrents.php?id=%s&json=1' % m)
    except URLError as e:
        return MessageContainer(title1="URL Error", title2="URL Error", message="Error fetching movie information: %s" % e, header="Url Error" % tID)

    match = re.search('\<h2.*\>(.*) \[(.*)\]', tostring(mHTML.xpath("id('content')/div/h2")[0]))
    oc.title1 = match.group(1)
    oc.title2 = match.group(2)

    # Genres
    mo_genres = []
    for elem in mHTML.xpath("id('content')/div/div[2]/div[3]/ul/li/a"):
        mo_genres.append(elem.text)

    # Synopsis
    try:
        mo_summary = mHTML.xpath("id('content')/div/div[3]/div[1]/div[2]")[0].text
    except IndexError:
        mo_summary = mHTML.xpath("id('content')/div/div[3]/div[2]/div[2]")[0].text

    # Hack to provide more viewing options for results
    oc.add(MovieObject(
            key = Callback(MovieInfo, m=m),
            rating_key = m,
            summary = mo_summary,
            genres = mo_genres,
            title = "Movie Info"
            ))

    rips = mJSON['Torrents']
    for r in rips:
        j = JSON.ObjectFromURL("https://tls.passthepopcorn.me/torrents.php?action=description&id=%s&torrentid=%s" % (m, r.get('Id')))
        images = HTML.ElementFromString(j['Description']).xpath('//img/@src')
        thumb = art = 'None'
        try:
            if Prefs['show_images'] == 'Cover' or Prefs['show_images'] == 'Both':
                thumb = String.Encode(images[0])
            if Prefs['show_images'] == 'Backgroun' or Prefs['show_images'] == 'Both':
                art = String.Encode(images[1])
        except IndexError:
            Log.Debug('Not enough images to display for rip %s' % r['Id'])
        title = ' / '.join([r['Codec'],r['Container'],r['Source'],r['Resolution'], sizeof_fmt(float(r['Size']))])
        if r.get('Scene'):
            title += ' / Scene'
        if r.get('GoldenPopcorn'):
            title += ' / GP'
        if r.get('FreeleechType'):
            title += ' / Freeleech!'
        summary = "%s\r\nSnatched\\Seeders\\Leechers: %s" % (r['ReleaseName'], '\\'.join([r['Snatched'], r['Seeders'], r['Leechers']]))
        oc.add(DirectoryObject(
                key = Callback(Torrent, tID=r['Id']),
                summary = summary,
                title = title,
                thumb = Callback(Thumb, url=thumb),
                art = Callback(Thumb, url=art)
                ))
    return oc

@route(PREFIX + '/movie/{m}')
def MovieInfo(m):
    mo = MovieObject()
    mHTML = HTML.ElementFromURL('https://tls.passthepopcorn.me/torrents.php?id=%s' % m)
    mJSON = JSON.ObjectFromURL('https://tls.passthepopcorn.me/torrents.php?id=%s&json=1' % m)

    # Title and year
    match = re.search('\<h2\>(.*) \[(.*)\]', tostring(mHTML.xpath("id('content')/div/h2")[0]))
    mo.title = match.group(1)
    mo.year = int(match.group(2))

    # Genres
    g = []
    for elem in mHTML.xpath("id('content')/div/div[2]/div[3]/ul/li/a"):
        match = re.search('\>(.*)\<', tostring(elem))
        g.append(match.group(1))
    if g:
        mo.genres = g
        mo.tagline = ','.join(g)

    # Cover
    match = re.search('src="(.*)" alt=', tostring(mHTML.xpath("id('content')/div/div[2]/div[1]/p/img")[0]))
    t = String.Encode(match.group(1))
    mo.thumb = Callback(Thumb, url=t)

    # Synopsis
    # This code makes me sad
    try:
        line = String.StripTags(tostring(mHTML.xpath("id('content')/div/div[3]/div[3]/div[2]")[0]))
    except IndexError:
        line = String.StripTags(tostring(mHTML.xpath("id('content')/div/div[3]/div[2]/div[2]")[0]))
    line = String.Quote(String.StripDiacritics(clean_html_ents(str(line))))
    match = re.search('%0A%09%09(.*?)%09%09%26', line)
    mo.summary = String.Unquote(match.group(1))

    rips = mJSON['Torrents']
    for r in rips:
        meo = MediaObject()
        meo.video_resolution

    mo.key = Callback(MovieInfo, m=mJSON["GroupId"])
    mo.rating_key = m

    meo = MediaObject(
            audio_channels = 2,
            audio_codec = AudioCodec.AAC,
            video_codec = VideoCodec.H264,
            container = Container.MKV,
            duration = 100465,
            video_resolution = 720,
            bitrate = 1500,
            aspect_ratio = 2,
            video_frame_rate =25,
            parts = [ PartObject(
                key=Callback(Thumb, url="garbage"))]
            )
    mo.add(meo)

    return mo

# Utilites

def sizeof_fmt(num):
    for x in ['bytes','KiB','MiB','GiB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num = num / 1024.0
    return "%3.1f %s" % (num, 'TB')

def clean_html_ents(string):
    return fromstring(fromstring(string).text).text

@route(PREFIX + '/torrent/{tID}')
def Torrent(tID):
    HTTP.Headers['User-Agent'] = "Wget/1.13.4"
    t = HTTP.Request("https://tls.passthepopcorn.me/torrents.php?action=download&id=%i" % int(tID))
    if t.headers['Content-Disposition']:
        localName = t.headers['Content-Disposition'].split('filename=')[1]
        if localName[0] == '"' or localName[0] == "'":
            localName = localName[1:-1]
        print localName
    else:
        localName = tID + '.torrent'
    data = HTTP.Request("https://tls.passthepopcorn.me/torrents.php?action=download&id=%i" % int(tID)).content
    try:
        fd = os.open(os.path.normpath(Prefs['save_dir']) + os.sep + localName , os.O_RDWR|os.O_CREAT)
        fo = os.fdopen(fd, "w+")
        fo.write(data)
        fo.close()
    except OSError as e:
        return MessageContainer(title1="Download", title2="Error", message="OS Error %s while downloading torrent %s." % (e, tID), header="Torrent %s" % tID)
    return MessageContainer(title1="Download", title2="Success", message="Torrent %s succesfully downloaded." % tID, header="Torrent %s" % tID)

@route(PREFIX + '/thumb/{url}')
def Thumb(url):
    if url == 'None':
        return Redirect(R(ICON))
    url = String.Decode(url)
    try:
        data = HTTP.Request(url, cacheTime = CACHE_1DAY).content
        return DataObject(data, 'image/jpeg')
    except:
        return Redirect(R(ICON))
